import React from 'react';
import { StyleSheet, Text, View ,TouchableOpacity} from 'react-native';
import {connect} from 'react-redux'

class CounterApp extends React.Component {

 
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() =>this.props.increaseCounter()}>
          <Text style={{fontSize:20}}>Increase</Text>
        </TouchableOpacity>
        <Text>{this.props.counter}</Text>
        <TouchableOpacity onPress={() => this.props.decreaseCounter()}>
        <Text style={{fontSize:20}}>Decrease</Text>
        </TouchableOpacity>
      </View>
    );
  }
}



 mapStateToProps =( state) => {
    return {
        counter:state.counter
    }
}

 mapDispatchToProps =(dispatch)=>{
    return{
        increaseCounter: () => dispatch({type:'INCREASE_COUNTER'}),
        decreaseCounter: () => dispatch({type:'DECREASE_COUNTER'}),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(CounterApp)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'row',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
});
